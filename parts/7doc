#!/bin/sh -e

# This file updates the doc/ directory of the web pages

# Oh, how I wish the -doc people would maintain this. -- Jay Treacy
# There ain't no such thing as the -doc people these days :/ -- Josip Rodin
# Let me refactor and add some recent docs: osamu

# ftpdir/sid is populated by 1ftpfiles with sid version of *.deb files

. `dirname $0`/../common.sh
webdocdir=$webdir/doc

#############################################################################
mkdirp()
{
test -d $1 || install -d -m 2775 $1
}

#############################################################################
# unpack all debs to cleaned $crondir/tmp
rm -rf $crondir/tmp
mkdirp $crondir/tmp
cd $crondir/tmp

#############################################################################
unpack()
{
namebin=$1     # binary package name: maint-guide-fr
dist=${2:-sid}
filedeb=`ls -t1 $ftpdir/${dist}/${namebin}_*.deb | head -1`
if [ -e ${filedeb}.stamp ]; then
  echo "I: Skip ${filedeb} (already unpacked)"
  touch $ftpdir/${dist}/${namebin}.skip
else
  # first time
  echo "I: Unpack ${filedeb}"
  dpkg-deb -x $filedeb .
  rm -f ${namebin}_*.deb.stamp
  touch ${filedeb}.stamp
  rm -f $ftpdir/${dist}/${namebin}.skip
fi
}

#############################################################################
mvdocs()
{
namedest=$1    # destdir directory:  maint-guide
basedir=$2     # binary package data dir.: usr/share/doc/maint-guide-fr
addlang=${3:-NO} # $lang in filename: NO | ADD | YES
 # NO:  without $lang and leave it so
 # ADD: without $lang and add it (make link for en)
 # YES: with    $lang and leave it so
lang=${4:-en}  # language name: en (default), fr, ...
sectdir=${5:-manuals/}  # destination top directory
                        # set to / or packaging-manuals/ if not
namedoc=${6:-$namedest}  # base filename w/o .txt or $lang.txt

destdir=$webdocdir/${sectdir}$namedest
mkdirp $destdir

if [ "${addlang}" = "YES" ]; then
	lang0=".$lang"
else # NO ADD
	lang0=""
fi

for ext in epub pdf txt text yaml ps; do
	sourcepath=$basedir/${namedoc}${lang0}.$ext
	if [ -f "`readlink -f $sourcepath.gz`" ]; then
		rm -f $sourcepath
		zcat `readlink -f $sourcepath.gz` >$sourcepath
	elif [ -f "`readlink -f $sourcepath.xz`" ]; then
		rm -f $sourcepath
		xzcat `readlink -f $sourcepath.xz` >$sourcepath
	fi
	if [ "$ext" = "text" ]; then
		ext="txt"
	fi
	if [ "${addlang}" = "NO" ]; then
		if [ -f "`readlink -f $sourcepath`" ]; then
			install -p -m 664 `readlink -f $sourcepath` $destdir/${namedoc}.$ext
		fi
	elif [ "${addlang}" = "ADD" ]; then
		if [ -f "`readlink -f $sourcepath`" ]; then
			install -p -m 664 `readlink -f $sourcepath` $destdir/${namedoc}.$lang.$ext
			if [ "$lang" = "en" ]; then
				ln -sf ${namedoc}.en.$ext $destdir/${namedoc}.$ext
			fi
		fi
	else # YES
		if [ -f "`readlink -f $sourcepath`" ]; then
			install -p -m 664 `readlink -f $sourcepath` $destdir/${namedoc}.$lang.$ext
		fi
	fi
done
}

#############################################################################
pagecopy()
{
# convert all internal href URLs from *.html to *.$lang.html
lang=$1
infile=$2
outfile=$3
sed -e "s/\(href=\"[^\/:\"= ]*\)\.html/\1.$lang.html/g" <$infile >$outfile
}

#############################################################################
mvhtml()
{
namedest=$1    # destdir directory:  maint-guide
basedir=$2     # binary package data dir.: usr/share/doc/maint-guide-fr/html
addlang=${3:-NO} # $lang in filename: NO | ADD | YES
 # NO:  without $lang and leave it so
 # ADD: without $lang and add it (make link for en) (internal URL conversion)
 # YES: with    $lang and leave it so (make link for en)
lang=${4:-en}  # language name: en (default), fr, ...
sectdir=${5:-manuals/}  # destination top directory
                        # set to / or packaging-manuals/ if not
namedoc=${6:-*}         # filemask w/o .html or .$lang.html

destdir=$webdocdir/${sectdir}$namedest
mkdirp $destdir

if [ "${addlang}" = "YES" ]; then
	lang0=".$lang"
else # NO ADD
	lang0=""
fi

pagepattern="$basedir/${namedoc}${lang0}.html"
for page in $pagepattern; do
	pagefile="`readlink -f $page`"
	if [ -f "$pagefile" ]; then
		if [ "${addlang}" = "ADD" ]; then
			if [ "$(basename $page .$lang.html)" = "$(basename $page)" ]; then
				# This is not *.$lang.html file but *.html
				pagecopy $lang "$pagefile" "$destdir/$(basename $page .html).$lang.html"
				if [ "$lang" = "en" ]; then
					ln -sf "$(basename $page .html).en.html" "$destdir/$(basename $page)"
				fi
			fi
		elif [ "${addlang}" = "YES" ]; then
			# This is *.$lang.html file
			install -p -m 664 `readlink -f $page` $destdir/$(basename $page)
			if [ "$lang" = "en" ]; then
				ln -sf "$(basename $page)" "$destdir/$(basename $page .en.html).html"
			fi
		else # NO This is *.html file
			install -p -m 664 `readlink -f $page` $destdir/$(basename $page)
		fi
	fi
done

if [ "$lang" = "en" ]; then
	pagepattern="$basedir/*.css"
	for page in $pagepattern; do
		if [ -f "`readlink -f $page`" ]; then
			install -p -m 664 `readlink -f $page` $destdir/$(basename $page)
		fi
	done
	pagepattern="$basedir/images/*"
	for page in $pagepattern ; do
		if [ -f "`readlink -f $page`" ]; then
			mkdirp $destdir/images
			install -p -m 664 `readlink -f $page` $destdir/images/$(basename $page)
		fi
	done
fi
}

#############################################################################
pagecopy2()
{
# convert all internal href URLs from *.html to *.$lang.html
# move language dependent images
lang=$1
infile=$2
outfile=$3
sed -e "s/\(href=\"[^\/:\"= ]*\)\.html/\1.$lang.html/g" \
    -e "s/\(<img src=\"images\)\//\1.$lang\//g" \
		<$infile >$outfile
}

#############################################################################
mvhtml2()
{
namedest=$1    # destdir directory: debian-handbook 
basedir=$2     # binary package data dir.: usr/share/doc/debian-handbook/html/$lang 
addlang=${3:-NO} # $lang in filename: NO | ADD | YES
 # NO:  without $lang and leave it so
 # ADD: without $lang and add it (make link for en) (internal URL conversion)
 # YES: with    $lang and leave it so (make link for en)
lang=${4:-en}  # language name: en (default), fr, ...
sectdir=${5:-manuals/}  # destination top directory
                        # set to / or packaging-manuals/ if not
namedoc=${6:-*}         # filemask w/o .html or .$lang.html

destdir=$webdocdir/${sectdir}$namedest
mkdirp $destdir

if [ "${addlang}" = "YES" ]; then
	lang0=".$lang"
else # NO ADD
	lang0=""
fi

pagepattern="$basedir/${namedoc}${lang0}.html"
for page in $pagepattern; do
	pagefile="`readlink -f $page`"
	if [ -f "$pagefile" ]; then
		if [ "${addlang}" = "ADD" ]; then
			if [ "$(basename $page .$lang.html)" = "$(basename $page)" ]; then
				# This is not *.$lang.html file but *.html
				pagecopy2 $lang "$pagefile" "$destdir/$(basename $page .html).$lang.html"
				if [ "$lang" = "en" ]; then
					ln -sf "$(basename $page .html).en.html" "$destdir/$(basename $page)"
				fi
			fi
		elif [ "${addlang}" = "YES" ]; then
			# This is *.$lang.html file
			install -p -m 664 `readlink -f $page` $destdir/$(basename $page)
			if [ "$lang" = "en" ]; then
				ln -sf "$(basename $page)" "$destdir/$(basename $page .en.html).html"
			fi
		else # NO This is *.html file
			install -p -m 664 `readlink -f $page` $destdir/$(basename $page)
		fi
	fi
done

# language dependent images
pagepattern="$basedir/images/*"
for page in $pagepattern ; do
	if [ -f "`readlink -f $page`" ]; then
		mkdirp $destdir/images.$lang
		install -p -m 664 `readlink -f $page` $destdir/images.$lang/$(basename $page)
	fi
done
if [ "$lang" = "en" ]; then
	pagepattern="$basedir/Common_Content/css/*.css"
	for page in $pagepattern; do
		if [ -f "`readlink -f $page`" ]; then
			mkdirp $destdir/Common_Content/css
			install -p -m 664 `readlink -f $page` $destdir/Common_Content/css/$(basename $page)
		fi
	done
	pagepattern="$basedir/Common_Content/images/*"
	for page in $pagepattern ; do
		if [ -f "`readlink -f $page`" ]; then
			mkdirp $destdir/Common_Content/images
			install -p -m 664 `readlink -f $page` $destdir/Common_Content/images/$(basename $page)
		fi
	done
fi
}

#############################################################################

mvhtml_sphinx()
{
# Copy of mvhtml(), modified so it copies the _images and _static subfolders too
# This is needed by debian-policy since they moved to reStructuredText and Sphinx
# This is probably uncomplete, since the _static folder contains symlinks to 
# some javascript that probably will not work. 

namedest=$1    # destdir directory:  maint-guide
basedir=$2     # binary package data dir.: usr/share/doc/maint-guide-fr/html
addlang=${3:-NO} # $lang in filename: NO | ADD | YES
 # NO:  without $lang and leave it so
 # ADD: without $lang and add it (make link for en) (internal URL conversion)
 # YES: with    $lang and leave it so (make link for en)
lang=${4:-en}  # language name: en (default), fr, ...
sectdir=${5:-manuals/}  # destination top directory
                        # set to / or packaging-manuals/ if not
namedoc=${6:-*}         # filemask w/o .html or .$lang.html

destdir=$webdocdir/${sectdir}$namedest
mkdirp $destdir

if [ "${addlang}" = "YES" ]; then
	lang0=".$lang"
else # NO ADD
	lang0=""
fi

pagepattern="$basedir/${namedoc}${lang0}.html"
for page in $pagepattern; do
	pagefile="`readlink -f $page`"
	if [ -f "$pagefile" ]; then
		if [ "${addlang}" = "ADD" ]; then
			if [ "$(basename $page .$lang.html)" = "$(basename $page)" ]; then
				# This is not *.$lang.html file but *.html
				pagecopy $lang "$pagefile" "$destdir/$(basename $page .html).$lang.html"
				if [ "$lang" = "en" ]; then
					ln -sf "$(basename $page .html).en.html" "$destdir/$(basename $page)"
				fi
			fi
		elif [ "${addlang}" = "YES" ]; then
			# This is *.$lang.html file
			install -p -m 664 `readlink -f $page` $destdir/$(basename $page)
			if [ "$lang" = "en" ]; then
				ln -sf "$(basename $page)" "$destdir/$(basename $page .en.html).html"
			fi
		else # NO This is *.html file
			install -p -m 664 `readlink -f $page` $destdir/$(basename $page)
		fi
	fi
done

if [ "$lang" = "en" ]; then
	pagepattern="$basedir/_static/*"
	for page in $pagepattern; do
		if [ -f "`readlink -f $page`" ]; then
			mkdirp $destdir/_static
			install -p -m 664 `readlink -f $page` $destdir/_static/$(basename $page)
		fi
	done
	pagepattern="$basedir/_images/*"
	for page in $pagepattern ; do
		if [ -f "`readlink -f $page`" ]; then
			mkdirp $destdir/_images
			install -p -m 664 `readlink -f $page` $destdir/_images/$(basename $page)
		fi
	done
        pagepattern="$basedir/_sources/*"
        for page in $pagepattern ; do
                if [ -f "`readlink -f $page`" ]; then
                        mkdirp $destdir/_sources
                        install -p -m 664 `readlink -f $page` $destdir/_sources/$(basename $page)
                fi
        done
fi
}

#############################################################################
lclocal()
{
case $1 in
zh_CN | zh-CN) echo "zh-cn" ;;
zh_TW | zh-TW) echo "zh-tw" ;;
pt_BR | pt-BR) echo "pt-br" ;;
*-*) echo "${1%%-*}" ;;
*_*) echo "${1%%_*}" ;;
*) echo "${1}" ;;
esac
}

#############################################################################
uplocal()
{
case $1 in
zh-cn) echo "zh_CN" ;;
zh-tw) echo "zh_TW" ;;
pt-br) echo "pt_BR" ;;
*) echo "$1" ;;
esac
}

#############################################################################
pkg2lang()
{
mask=$1 # aptitude-doc- w/o *
dist=${2:-sid}
cd  $ftpdir/${dist} >/dev/null
ls -1 $mask*.deb | \
sed -e "s/^$mask//g" -e 's/_.*\.deb$//g' | \
sort | uniq
cd - >/dev/null
}

#############################################################################
file2lang()
{
mask=$1 # debmake-doc w/o *.txt.gz
ext=${2:-.txt.gz}
if [ -z "$3" ]; then
directory="usr/share/doc/${mask}"
else
directory="$3"
fi
cd  $crondir/tmp/$directory >/dev/null
ls -1 ${mask}.*${ext} | \
sed -e "s/^${mask}\.//g" -e "s/$ext\$//g"
cd - >/dev/null
}

#############################################################################
dir2lang()
{
directory="$1"
cd  $crondir/tmp/$directory >/dev/null
ls -1d * ; \
cd - >/dev/null
}

#############################################################################
echo -n "Installing documents:"

# We only have sid now
dist=sid

# Debian Policy Manual
unpack debian-policy
if [ ! -e $ftpdir/${dist}/debian-policy.skip ]; then
mvdocs debian-policy usr/share/doc/debian-policy NO en / policy
mvhtml_sphinx debian-policy usr/share/doc/debian-policy/policy.html NO en /

mvdocs fhs usr/share/doc/debian-policy/fhs NO en packaging-manuals/ fhs-3.0
mvhtml fhs usr/share/doc/debian-policy/fhs NO en packaging-manuals/ fhs-3.0

mvdocs menu-policy usr/share/doc/debian-policy NO en packaging-manuals/
mvhtml menu-policy usr/share/doc/debian-policy/menu-policy.html NO en packaging-manuals/

mvdocs perl-policy usr/share/doc/debian-policy NO en packaging-manuals/
mvhtml perl-policy usr/share/doc/debian-policy/perl-policy.html NO en packaging-manuals/

mvhtml packaging-manuals usr/share/doc/debian-policy NO en / debconf_specification
mvdocs packaging-manuals usr/share/doc/debian-policy NO en / libc6-migration
mvdocs packaging-manuals usr/share/doc/debian-policy NO en / upgrading-checklist
mvdocs packaging-manuals usr/share/doc/debian-policy NO en / virtual-package-names-list

mvdocs packaging-manuals usr/share/doc/debian-policy NO en / copyright-format-1.0
mvhtml packaging-manuals usr/share/doc/debian-policy NO en / copyright-format-1.0
# Move the copyright-format files to its canonical place
mkdirp $webdocdir/packaging-manuals/copyright-format/1.0
mv -f $webdocdir/packaging-manuals/copyright-format-1.0.html $webdocdir/packaging-manuals/copyright-format/1.0/index.html
mv -f $webdocdir/packaging-manuals/copyright-format-1.0.txt  $webdocdir/packaging-manuals/copyright-format/1.0/copyright-format.txt
fi

# Other policy documents
unpack build-essential
if [ ! -e $ftpdir/${dist}/build-essential.skip ]; then
install -p -m 664 usr/share/doc/build-essential/list $webdocdir/packaging-manuals/build-essential
fi

unpack menu
if [ ! -e $ftpdir/${dist}/menu.skip ]; then
mvhtml menu.html usr/share/doc/menu/html NO en packaging-manuals/
fi

unpack emacsen-common
if [ ! -e $ftpdir/${dist}/menu.skip ]; then
gunzip usr/share/doc/emacsen-common/debian-emacs-policy.gz
install -p -m 664 usr/share/doc/emacsen-common/debian-emacs-policy $webdocdir/packaging-manuals
fi

unpack java-policy
if [ ! -e $ftpdir/${dist}/java-policy.skip ]; then
rm -rf $webdocdir/packaging-manuals/java-policy
mkdir $webdocdir/packaging-manuals/java-policy
mvhtml java-policy     usr/share/doc/java-policy/debian-java-policy NO en packaging-manuals/
mvhtml debian-java-faq usr/share/doc/java-policy/debian-java-faq NO en
fi

unpack python3-dev
if [ ! -e $ftpdir/${dist}/python3-dev.skip ]; then
mvhtml_sphinx python-policy usr/share/doc/python3/ NO en packaging-manuals/
mv $webdocdir/packaging-manuals/python-policy/python-policy.html $webdocdir/packaging-manuals/python-policy/index.html
install -p -m 664 usr/share/doc/python3/searchindex.js $webdocdir/packaging-manuals/python-policy/
fi

# Other manuals
unpack debian-refcard
if [ ! -e $ftpdir/${dist}/debian-refcard.skip ]; then
# Too irregular to handle with mvdocs
for refcardfile in usr/share/doc/debian-refcard/*a4.pdf.gz ; do
	# rename from refcard-fr-a4.pdf.gz to refcard.fr.pdf as expected by the website
	futurename=`echo $refcardfile | sed 's/refcard-\(\w*\)-a4.pdf.gz/refcard.\1.pdf/'`
	gunzip -f $refcardfile && mv usr/share/doc/debian-refcard/$(basename $refcardfile .gz) $futurename
done
mkdirp $webdocdir/manuals/refcard
install -p -m 664 usr/share/doc/debian-refcard/*pdf $webdocdir/manuals/refcard/
fi

unpack packaging-tutorial
if [ ! -e $ftpdir/${dist}/packaging-tutorial.skip ]; then
mvdocs packaging-tutorial usr/share/doc/packaging-tutorial ADD en
langlist=`file2lang packaging-tutorial .pdf`
for lang in $langlist ; do
	mvdocs packaging-tutorial usr/share/doc/packaging-tutorial YES $lang
done
fi

unpack developers-reference
if [ ! -e $ftpdir/${dist}/developers-reference.skip ]; then
mvdocs developers-reference usr/share/doc/developers-reference/docs ADD en
mvhtml_sphinx developers-reference usr/share/doc/developers-reference/docs ADD en
langlist=`pkg2lang developers-reference-`
for lang in $langlist ; do
	unpack developers-reference-$lang
	mvdocs developers-reference usr/share/doc/developers-reference/docs/$lang ADD $lang
	mvhtml_sphinx developers-reference usr/share/doc/developers-reference/docs/$lang ADD $lang
done
fi

unpack debian-faq
if [ ! -e $ftpdir/${dist}/debian-faq.skip ]; then
mvdocs debian-faq usr/share/doc/debian/FAQ YES en
mvhtml debian-faq usr/share/doc/debian/FAQ YES en
langlist=`pkg2lang debian-faq-`
for lang in $langlist ; do
	unpack debian-faq-$lang
	mvdocs debian-faq usr/share/doc/debian/FAQ YES $lang
	ln -sf debian-faq.zh-cn.pdf $webdocdir/manuals/debian-faq/debian-faq.zh_CN.pdf
	ln -sf debian-faq.zh-cn.txt $webdocdir/manuals/debian-faq/debian-faq.zh_CN.txt
	mvhtml debian-faq usr/share/doc/debian/FAQ/$lang YES $lang
done
fi

unpack maint-guide
if [ ! -e $ftpdir/${dist}/maint-guide.skip ]; then
mvdocs maint-guide usr/share/doc/maint-guide YES en
mvhtml maint-guide usr/share/doc/maint-guide/html YES en
langlist=`pkg2lang maint-guide-`
# ko pl pt-br zh-cn zh-tw are old (zh-tw and zh-tw is in NEW)
# once NEW pass
langlist=`echo $langlist | sed -e s/ko// -e s/pl// -e s/pt-br//`
for lang in $langlist ; do
	unpack maint-guide-$lang
	mvdocs maint-guide usr/share/doc/maint-guide-$lang YES $lang
	mvhtml maint-guide usr/share/doc/maint-guide-$lang/html YES $lang
done
fi

unpack debian-reference-common
if [ ! -e $ftpdir/${dist}/debian-reference-common.skip ]; then
langlist=`pkg2lang debian-reference-`
for lang in $langlist ; do
	unpack debian-reference-$lang
	mvdocs debian-reference usr/share/debian-reference YES $lang 
	mvhtml debian-reference usr/share/debian-reference YES $lang
done
fi

unpack debmake-doc
if [ ! -e $ftpdir/${dist}/debmake-doc.skip ]; then
langlist=`file2lang debmake-doc`
for lang in $langlist ; do
	mvdocs debmake-doc usr/share/doc/debmake-doc YES $lang
	mvhtml debmake-doc usr/share/doc/debmake-doc/html YES $lang
done
fi

unpack apt-doc
if [ ! -e $ftpdir/${dist}/apt-doc.skip ]; then
mvdocs apt-guide usr/share/doc/apt-doc YES en manuals/ guide
mvhtml apt-guide usr/share/doc/apt-doc/guide.html ADD en
langlist=`file2lang guide .text.gz usr/share/doc/apt-doc`
for lang in $langlist ; do
	mvdocs apt-guide usr/share/doc/apt-doc YES $lang manuals/ guide
	ln -sf guide.$lang.txt $webdocdir/manuals/apt-guide/apt-guide.$lang.txt
	mvhtml apt-guide usr/share/doc/apt-doc/guide.$lang.html ADD $lang
done
mvdocs apt-offline usr/share/doc/apt-doc YES en manuals/ offline
mvhtml apt-offline usr/share/doc/apt-doc/offline.html ADD en 
langlist=`file2lang offline .text.gz usr/share/doc/apt-doc`
for lang in $langlist ; do
	mvdocs apt-offline usr/share/doc/apt-doc YES $lang manuals/ offline
	ln -sf offline.$lang.txt $webdocdir/manuals/apt-offline/apt-offline.$lang.txt
	mvhtml apt-offline usr/share/doc/apt-doc/offline.$lang.html ADD $lang
done
fi

langlist=`pkg2lang aptitude-doc-`
for lang in $langlist ; do
	unpack aptitude-doc-$lang
if [ ! -e $ftpdir/${dist}/aptitude-doc-$lang.skip ]; then
	mvhtml aptitude usr/share/doc/aptitude/html/$lang ADD $lang
fi
done

unpack debian-handbook
if [ ! -e $ftpdir/${dist}/debian-handbook.skip ]; then
langlist=`dir2lang usr/share/doc/debian-handbook/html`
for lang in $langlist ; do
	lclang=`lclocal $lang`
	mvhtml2 debian-handbook usr/share/doc/debian-handbook/html/$lang ADD $lclang
done
fi

unpack dbconfig-common
if [ ! -e $ftpdir/${dist}/dbconfig-common.skip ]; then
mvdocs dbapp-policy usr/share/doc/dbconfig-common NO en manuals/ dbapp-policy
mvdocs dbconfig-common usr/share/doc/dbconfig-common NO en manuals/ dbconfig-common
mvhtml dbapp-policy usr/share/doc/dbconfig-common/dbapp-policy.html NO en
mvhtml dbconfig-common usr/share/doc/dbconfig-common NO en
mvhtml dbconfig-common usr/share/doc/dbconfig-common/dbconfig-common.html NO en
fi

unpack debian-history
if [ ! -e $ftpdir/${dist}/debian-history.skip ]; then
mvhtml project-history usr/share/doc/debian-history/docs YES en
langlist=`file2lang project-history .pdf /usr/share/doc/debian-history/docs`
for lang in $langlist ; do
        mvdocs project-history usr/share/doc/debian-history/docs YES $lang manuals/ project-history
        mvhtml project-history usr/share/doc/debian-history/docs YES $lang
done
fi

unpack hamradio-maintguide
if [ ! -e $ftpdir/${dist}/hamradio-maintguide.skip ]; then
mvhtml_sphinx hamradio-maintguide usr/share/doc/hamradio-maintguide/html ADD en
install -p -m 664 usr/share/doc/hamradio-maintguide/html/objects.inv $webdocdir/manuals/hamradio-maintguide
install -p -m 664 usr/share/doc/hamradio-maintguide/html/searchindex.js $webdocdir/manuals/hamradio-maintguide
gunzip -c usr/share/doc/hamradio-maintguide/DebianHamradio.pdf.gz >usr/share/doc/hamradio-maintguide/hamradio-maintguide.pdf
mvdocs hamradio-maintguide usr/share/doc/hamradio-maintguide/ ADD en
fi

unpack harden-doc
if [ ! -e $ftpdir/${dist}/harden-doc.skip ]; then
langlist=`dir2lang usr/share/doc/harden-doc/html`
for lang in $langlist ; do
        gunzip usr/share/doc/harden-doc/html/$lang/changelog.html.gz
done
for lang in $langlist ; do
        lclang=`lclocal $lang`
        mvhtml2 securing-debian-manual usr/share/doc/harden-doc/html/$lang ADD $lclang
done
fi

# Move broken code segment here for debug
#echo
#echo "7doc start debug (at `date`)"
#set -x
###
### MOVE PROBLEMATIC CODE to here
###
#set +x

echo
echo "7doc finished (at `date`)"

echo


